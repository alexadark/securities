<?php
namespace App\Models;

use \TypeRocket\Models\WPPost;

class Person extends WPPost
{
    protected $postType = 'tr_team';

	protected $fillable = [
		'photo',
		'post_content',
		'job_title'
	];

	protected $format = [
		'photo' => 'intval' // set the photo field to an integer
	];


}