<?php
/**
 * Securities.
 *
 * This file adds functions to the Genesis Webstantly Starter Theme.
 *
 * @package Webstantly starter
 * @author  Alexandra Spalato
 * @license GPL-2.0+
 * @link    http://alexandraspalato.com/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

//Initialize theme constants

$child_theme = wp_get_theme();

define( 'CHILD_THEME_NAME', $child_theme->get( 'Name' ) );
define( 'CHILD_THEME_URL', $child_theme->get( 'ThemeURI' ) );
define( 'CHILD_THEME_VERSION', $child_theme->get( 'Version' ) );
define( 'CHILD_TEXT_DOMAIN', $child_theme->get( 'TextDomain' ) );

define( 'CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'CHILD_URI', get_stylesheet_directory_uri() );
define( 'CHILD_CONFIG_DIR', CHILD_THEME_DIR . '/config/' );
define( 'CHILD_LIB', CHILD_THEME_DIR . '/lib/' );
define( 'CHILD_IMG', CHILD_URI . '/assets/images/' );
define( 'CHILD_JS', CHILD_URI . '/assets/js/' );

// Setup Theme.
include_once( CHILD_THEME_DIR . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'wst_localization_setup' );
function wst_localization_setup() {
	load_child_theme_textdomain( 'genesis-sample', CHILD_THEME_DIR . 'assets/languages' );
}

// Add the helper functions.
include_once( CHILD_THEME_DIR . '/lib/helper-functions.php' );


/*-----------------------------------------------------------
	LOAD ASSETS
/*------------------------------------------------------------*/

// Enqueue Scripts and Styles.
add_action( 'wp_enqueue_scripts', 'wst_enqueue_scripts_styles' );
/**
 * Enqueue styles and scripts
 *
 * @since 1.0.0
 *
 */
function wst_enqueue_scripts_styles() {

	wp_enqueue_style( 'genesis-sample-fonts', '//fonts.googleapis.com/css?family=Lato:300,400|Open+Sans:300,400,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );

	wp_enqueue_script( 'uikit-js', '//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.27/js/uikit.min.js', array( 'jquery' ), '3.0.0-beta.28', true );

	wp_enqueue_script( 'uikit-icons-js', '//cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.27/js/uikit-icons.min.js',
		array( 'uikit-js' ),
		'3.0.0-beta.28', true );
	wp_enqueue_script( 'theme-js', CHILD_JS . 'theme.js', array(
		'jquery',
	), CHILD_THEME_VERSION, true );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'genesis-sample-responsive-menu', CHILD_JS . "responsive-menus{$suffix}.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'genesis-sample-responsive-menu',
		'genesis_responsive_menu',
		wst_responsive_menu_settings()
	);

}

add_action( 'wp_enqueue_scripts', 'wst_enqueue_slider_scripts_styles' );
/**
 * Enqueue styles and scripts for swiper slider if we are on front page/
 *
 * @since 1.0.0
 *
 */
function wst_enqueue_slider_scripts_styles() {
	if ( ! is_front_page() ) {
		return;
	}
	wp_enqueue_style( 'swiper-css', CHILD_URI . '/assets/slider/swiper.min.css', array(), '3.4.2' );
	wp_enqueue_script( 'swiper-js', CHILD_URI . '/assets/slider/swiper.jquery.min.js', array( 'jquery' ), '3.4.2', true );
	wp_enqueue_script( 'swiper-init', CHILD_URI . '/assets/slider/swiper.init.js', array( 'swiper-js' ), '1',
		true );

}

// Define our responsive menu settings.
function wst_responsive_menu_settings() {

	$settings = array(
		'mainMenu'          => __( 'Menu', 'genesis-sample' ),
		'menuIconClass'     => 'dashicons-before dashicons-menu',
		'subMenu'           => __( 'Submenu', 'genesis-sample' ),
		'subMenuIconsClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'       => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

add_action( 'wp_enqueue_scripts', 'wst_enqueue_backstretch' );
function wst_enqueue_backstretch() {
	if ( is_front_page() || is_page( 'contact-us' ) ) {
		return;
	}

	wp_enqueue_script( 'backstretch', CHILD_URI . '/assets/js/jquery.backstretch.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'backstretch-set', CHILD_URI . '/assets/js/backstretch-set.js', array( 'backstretch' ), '1.0.0', true );
//
//	$featured_image_url = is_home()? get_field('blog_header_image','option') :  wp_get_attachment_url(
//		get_post_thumbnail_id() );

	if ( is_home() ) {
		$backstretch_src = array( 'src' => get_field( 'blog_header_image', 'option' ) );
	} elseif ( is_singular( 'post' ) ) {
		$backstretch_src = array( 'src' => get_field( 'single_top_image', 'option' ) );
	} else {
		$backstretch_src = array( 'src' => wp_get_attachment_url( get_post_thumbnail_id() ) );

	}

	wp_localize_script( 'backstretch-set', 'BackStretchImg', $backstretch_src );


}

// Change order of main stylesheet to override plugin styles.
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
add_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet', 99 );

/*-----------------------------------------------------------
	THEME SUPPORTS
/*------------------------------------------------------------*/

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array(
	'404-page',
	'drop-down-menu',
	'headings',
	'rems',
	'search-form',
	'skip-links'
) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );

// Add support for custom header.
add_theme_support( 'custom-header', array(
	'width'           => 305,
	'height'          => 54,
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'flex-height'     => true,
) );

// Add support for custom background.
add_theme_support( 'custom-background' );

// Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Add support for 3-column footer widgets.
//add_theme_support( 'genesis-footer-widgets', 3 );

//add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'site-inner',
	'header',
	'nav',
	'subnav',
	'footer-widgets',
	'footer'
) );

/*-----------------------------------------------------------
	REMOVE SITE LAYOUTS
/*------------------------------------------------------------*/

genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-content-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );

unregister_sidebar( 'sidebar-alt' );


/*-----------------------------------------------------------
	IMAGE SIZES
/*------------------------------------------------------------*/

add_image_size( 'featured-image', 436, 298, true );
/*-----------------------------------------------------------
	WIDGET AREAS
/*------------------------------------------------------------*/

genesis_register_widget_area(
	array(
		'id'          => 'top-bar',
		'name'        => __( 'Top Bar', CHILD_TEXT_DOMAIN ),
		'description' => __( 'This is the top bar widgets section', CHILD_TEXT_DOMAIN ),
	)
);
genesis_register_widget_area(
	array(
		'id'          => 'footer',
		'name'        => __( 'Footer', CHILD_TEXT_DOMAIN ),
		'description' => __( 'This is the footer widgets section', CHILD_TEXT_DOMAIN ),
	)
);


/*-----------------------------------------------------------
	NAV
/*------------------------------------------------------------*/

// Rename primary and secondary navigation menus.
add_theme_support( 'genesis-menus', array(
	'primary'   => __( 'After Header Menu', 'genesis-sample' ),
	'secondary' => __( 'Footer Menu', 'genesis-sample' )
) );

// Reposition the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

// Reduce the secondary navigation menu to one level depth.
add_filter( 'wp_nav_menu_args', 'wst_secondary_menu_args' );
function wst_secondary_menu_args( $args ) {

	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;

	return $args;

}

/*-----------------------------------------------------------
	GRAVATAR
/*------------------------------------------------------------*/


// Modify size of the Gravatar in the author box.
add_filter( 'genesis_author_box_gravatar_size', 'wst_author_box_gravatar' );
function wst_author_box_gravatar( $size ) {
	return 90;
}

// Modify size of the Gravatar in the entry comments.
add_filter( 'genesis_comment_list_args', 'wst_comments_gravatar' );
function wst_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;

	return $args;

}

/*-----------------------------------------------------------
	HEADER
/*------------------------------------------------------------*/

if ( ! wp_is_mobile() ) {
	add_filter( 'genesis_attr_nav-primary', 'wst_change_site_header_attr', 99 );
}
/**
 * Sticky menu on desktop
 *
 * @since 1.0.0
 *
 * @param string $attr sticky attribute
 *
 *
 */
function wst_change_site_header_attr( $attr ) {

	$attr['uk-sticky'] = ' ';

	return $attr;
}

add_action( 'genesis_before_header', 'wst_display_top_bar' );
/**
 * Display top bar
 *
 * @since 1.0.0
 *
 */
function wst_display_top_bar() {
	genesis_widget_area( 'top-bar', array(
			'before' => '<div id="top-bar"><div class="top-bar wrap">',
			'after'  => '</div></div>'

		)
	);
    ?>
    <div class="top-right-box"><div class="wrap"><div class="box">
    <?php
        echo do_action('wpml_add_language_selector');
        echo get_field( 'top_right_content', 'option' );
    echo '</div></div></div>';
}

add_action( 'genesis_before_header', 'wst_open_header_wrap', 15 );
/**
 * Open header wrap around header slider and top image
 *
 * @since 1.0.0
 *
 */
function wst_open_header_wrap() {
	echo '<div class="header-wrap">';
}

add_action( 'genesis_after_header', 'wst_close_header_wrap', 15 );
/**
 * Open header wrap around header slider and top image
 *
 * @since 1.0.0
 *
 */
function wst_close_header_wrap() {
	echo '</div>';
}


/*-----------------------------------------------------------
	SLIDER
/*------------------------------------------------------------*/

add_action( 'genesis_after_header', 'wst_display_home_slider' );
function wst_display_home_slider() {
	if ( ! is_front_page() ) {
		return;
	}
	$context   = Timber::get_context();
	$templates = array( 'front-page/slider.twig' );
	Timber::render( $templates, $context );
}


/*-----------------------------------------------------------
	POST
/*------------------------------------------------------------*/
//Remove archive title description
remove_action( 'genesis_before_loop', 'genesis_do_posts_page_heading' );


//Reposition featured image in archive.

remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );

add_action( 'genesis_entry_header', 'genesis_do_post_image', 5 );

//add_action( 'genesis_before_entry', 'wst_show_featured_image_single_post' );

/**
 * Display featured image (if present) before entry on single Posts
 */

function wst_show_featured_image_single_post() {
	if ( ! ( is_singular( 'post' ) && has_post_thumbnail() ) ) {
		return;
	}

	$args = array(
		'size' => 'featured-image',
		'attr' => array(
			'class' => 'featured-image',
		),
	);
	genesis_image( $args );

}

/*-----------------------------------------------------------
	FOOTER
/*------------------------------------------------------------*/
add_action( 'genesis_before_footer', 'wst_display_fat_footer' );
function wst_display_fat_footer() {
	$context   = Timber::get_context();
	$templates = array( 'fat-footer.twig' );
	Timber::render( $templates, $context );


}

//* Customize the entire footer
remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'wst_custom_footer' );
function wst_custom_footer() {
	genesis_widget_area( 'footer', array(
		'before' => '<div id="footer" class="uk-text-center">',
		'after'  => '</div>'
	) );
}



