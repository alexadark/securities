<?php
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );

//Reposition post title before sidebar
remove_action('genesis_entry_header','genesis_do_post_title');
add_action('genesis_before_content_sidebar_wrap','genesis_do_post_title');

// Customize entry meta header
add_filter( 'genesis_post_info', 'themeprefix_post_info_filter' );
function themeprefix_post_info_filter( $post_info ) {
	$post_info = '[post_date] By [post_author_posts_link] 
[post_edit]';
	return $post_info;
}

// Customize entry meta footer
add_filter( 'genesis_post_meta', 'themeprefix_post_meta_filter' );
function themeprefix_post_meta_filter( $post_meta ) {
	$post_meta = '[post_categories before="Categories: "] [post_tags before=" Tags: "]';
	return $post_meta;
}




genesis();