<?php
//Template Name: Key Parties page

remove_action( 'genesis_loop', 'genesis_do_loop' );
add_action( 'genesis_loop', 'wst_display_key_parties' );
function wst_display_key_parties() {
	$context   = Timber::get_context();
	$templates = array( 'key-parties.twig' );
	Timber::render( $templates, $context );
}


genesis();