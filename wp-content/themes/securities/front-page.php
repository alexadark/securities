<?php

remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_after_header', 'wst_display_investment_vehicles_section', 20 );
function wst_display_investment_vehicles_section() {
	$context   = Timber::get_context();
	$templates = array( 'front-page/investment-vehicles.twig' );
	Timber::render( $templates, $context );
}
add_action( 'genesis_after_header', 'wst_display_benefits_section', 21 );
function wst_display_benefits_section() {
	$context   = Timber::get_context();
	$templates = array( 'front-page/benefits.twig' );
	Timber::render( $templates, $context );
}
add_action( 'genesis_after_header', 'wst_display_awards_section', 21 );
function wst_display_awards_section() {
	$context   = Timber::get_context();
	$templates = array( 'front-page/awards.twig' );
	Timber::render( $templates, $context );
}

genesis();