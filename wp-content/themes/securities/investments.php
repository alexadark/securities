<?php
//Template Name: Investments page
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );

add_action( 'genesis_entry_footer', 'wst_display_investment_options' );
function wst_display_investment_options() {
	$context   = Timber::get_context();
	$templates = array( 'investments.twig' );
	Timber::render( $templates, $context );

}


genesis();