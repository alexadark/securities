/**
 * Swiper init 1
 *
 */

jQuery(document).ready(function ($) {
    //initialize swiper when document ready
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        direction: 'vertical',
        autoplay: 0,
        speed: 1000
    });
});
