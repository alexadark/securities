jQuery(document).ready(function ($) {
    function equalHeight(boxes){
        $.each(boxes, function(){
            var box = $(this),
                highestBox = 0;

            box.each(function(){
                if($(this).outerHeight()>highestBox){
                    highestBox = $(this).outerHeight();
                }
            });
            box.height(highestBox);
        });
    }

    function equalHeightPerThree(boxes){
        var loop = 0,
            current_boxes = [];
        $.each(boxes, function(){
            var box = $(this),
                highestBox = 0;

            box.each(function(){
                if($(this).outerHeight()>highestBox){
                    highestBox = $(this).outerHeight();
                }
                current_boxes.push($(this));
                if(loop>=2) {
                    $.each(current_boxes, function(){
                        $(this).height(highestBox);
                    });
                    highestBox = 0;
                    loop = 0;
                    current_boxes = [];
                }
                loop++;
            });
            $.each(current_boxes, function(){
                $(this).height(highestBox);
            });
        });
    }

    $('.blog .entry-content').each(function(){
        var $morelink = $(this).find('p a.more-link');
        $morelink.appendTo($(this).next());
    });

    var boxes = new Array($('.small-block-content'), $('.small-block-title'), $('.first-blurb'), $('.blog .entry-headerx'), $('.blog .entry-content p'), $('.option-list h4'), $('.option-content'));
    var threeBox = new Array($('.video-entry p'));
    if($(window).width()>=768){
        equalHeight(boxes);
        equalHeightPerThree(threeBox);
    }

    $(window).resize(function(){
        if($(window).width()>=768){
            equalHeight(boxes);
            equalHeightPerThree(threeBox);
        } else {
            $.each(boxes, function(){
                $(this).outerHeight('auto');
            });
            $.each(threeBox, function(){
                $(this).outerHeight('auto');
            });
        }
    });


});